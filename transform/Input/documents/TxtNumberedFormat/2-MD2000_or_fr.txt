﻿1. LE MONDE DIPLOMATIQUE - DÉCEMBRE 1999  
2. ZONES FRANCHES ET RIDEAU DE FER
3. Entre le Mexique et les Etats-Unis, plus qu’une frontière
4. SUR la “ Frontera ”, l’espace économique où résident environ 8 millions d’habitants, s’étend le grand marché du monde en pleine expansion industrielle, celui de ces métropoles jumelles binationales, prototypes d’une économie globalisée.
5. Côté mexicain, l’exploitation des travailleurs y est la règle, dans les usines de sous- traitance ou “ maquiladoras ”.
6. Des masses d’immigrés de l’intérieur y débarquent, qui se retrouvent pris au piège et n’auront, une fois constatée l’extrême précarité de leurs conditions de vie, qu’une seule solution : passer la frontière.
7. Mais, si les biens et produits financiers circulent librement, des moyens importants sont mis en oeuvre par le gouvernement des Etats-Unis pour enrayer le passage des hommes. 
8. Par JANETTE HABEL
9. De San Diego (Etats-Unis) à Tijuana (Mexique), le trolley rouge met quarante-cinq minutes jusqu’au point de passage frontalier.
10. Les noms espagnols des arrêts (Chula Vista, San Isidro, El Cajon, Morena, Linda Vista) rappellent qu’au XIXe siècle ce territoire fut mexicain.
11. Aux pelouses bien entretenues des 70 terrains de golf de San Diego succèdent les rues sales de Tijuana, encombrées de vendeurs ambulants qui harcèlent les touristes.
12. La zone urbaine est continue. A peine descendu du trolley, un bus mexicain vous amène au centre-ville sans même vérifier vos documents. Les formalités sont réduites au minimum.
13. Mais au retour, pour peu que les papiers ne soient pas en règle, la douane américaine ne plaisante pas.
14. Depuis les années 70, la frontière nord du Mexique connaît des transformations spectaculaires.
15. La croissance démographique a transformé des petites villes en métropoles urbaines. La population de Tijuana est passée de 65 000 habitants, en 1950, à 1,3 million aujourd’hui, une croissance supérieure à celle de sa soeur jumelle (et américaine), San Diego.
16. Le territoire frontalier attire, car il est devenu l’un des principaux pôles industriels du pays.
17. Depuis 1965, mais encore plus depuis la ratification de l’Accord de libre-échange nord-américain (Alena), de nombreuses multinationales américaines, japonaises et coréennes se sont installées “ en grappes ” dans les parcs industriels du nord du Mexique. 
18. De 1970 à 1975, la production a augmenté de 4,9 % par an en moyenne, puis de 12 % la décennie suivante.
19. Sa croissance actuelle est d’environ 11 %. Les maquiladoras sont à l’origine de cet essor.
20. Plus d’un million de personnes travaillent dans cette industrie d’assemblage, installée le long de la frontière, qui bénéficie d’un régime fiscal particulier pour l’exportation et permet des coûts de fabrication jusqu’à dix fois inférieurs à ceux des Etats-Unis.
21. En 1998, ces usines ont exporté des marchandises pour une valeur de 55 milliards de dollars.
22. L’activité productive est dynamique.
23. Il y avait 719 maquiladoras recensées en 1996 dans 25 parcs industriels.
24. A Tijuana, surnommée “ Tivijuana ” pour sa spécialisation dans les téléviseurs, Samsung, Sony, Hyundai, Sanyo embauchent.
25. La région agit comme un véritable aimant pour les déshérités du Mexique et d’Amérique centrale (lire, ci-dessous, l’article de Marie-Agnès Combesque).
26. Poussés par le chômage et les bas salaires, les habitants des Etats les plus pauvres fournissent les plus gros contingents de travailleurs migrants ; 35 à 40 % sont des indigènes.
27. Ce réservoir de force de travail bon marché à la recherche des “ migradollars ” envoyés aux familles est fonctionnel.
28. Il permet de déconnecter l’évolution de la productivité et des rémunérations : 450 à 675 pesos pour dix heures de travail par jour et une semaine de six jours . 
29. Grâce au taux très élevé de rotation de la main-d’oeuvre (jeune, féminine et migrante), les salaires n’augmentent pas, ce à quoi contribue le corporatisme des syndicats officiels mexicains, dont les cadres imposent par la contrainte la gestion clientéliste de bonnes relations dans l’entreprise.
30. La flexibilité du travail est plus grande ici que dans le reste du pays.
31. Le plus souvent, la vie syndicale est absente dans les ateliers et l’existence de conventions collectives est ignorée.
32. Lorsque des travailleurs protestent, la répression est brutale, entraînant des licenciements massifs, la fermeture et le transfert d’usines vers des territoires plus coopératifs.
33. Les ressources en capital et l’ampleur du marché américain conditionnent l’économie mexicaine, dont la subordination au dollar est croissante. 
34. Le chercheur mexicain Ramon Eduardo Ruiz conteste les affirmations des partisans de la théorie du développement d’un nouveau modèle, celui de la frontière nord, selon lesquelles le développement de la zone frontalière permet d’intégrer des secteurs modernes et arriérés dans une économie binationale.
35. Les maquiladoras sont une enclave industrielle souvent extérieure à l’économie locale, tributaire des liens internationaux - en 1998, 97,3 % des intrants étaient importés - même si la compétitivité de l’économie régionale bénéficie de la flexibilité et de la malléabilité de la force de travail.
36. Mais l’abondance de main-d’oeuvre bon marché n’est pas un avantage comparatif équivalent à l’afflux des capitaux, de la technologie et du pouvoir d’achat du grand marché américain.
37. Prétendre qu’il s’agirait là d’un bénéfice réciproque et d’une interdépendance fructueuse n’est qu’un mythe.
38. Pour Ruiz, “ la réalité de la frontière nord, c’est la dépendance, une dépendance structurelle ”, à l’opposé d’un développement durable.
39. Loin d’être l’amorce d’une restructuration productive dessinant les contours d’une nouvelle relation Nord-Sud comme l’affirment les économistes néolibéraux de Mexico, il s’agit d’une insertion internationale fondée sur la compétitivité du sous-développement, sur l’asymétrie entre l’économie la plus puissante du monde et celle d’un pays semi-industrialisé.
40. L’absence de normes et de réglementation a des effets désintégrateurs, dont le coût social est incalculable.
41. Ainsi, l’intégration transfrontalière aggrave l’ampleur des dégâts écologiques.
42. Si les usines mexicaines ne se privent pas de déverser leurs eaux usées infectées de bactéries et de virus dans les égouts californiens, les firmes américaines renvoient vers le sud trente fois plus de déchets toxiques, en dépit des régulations de l’Alena.
43. L’usine Samsung à Tijuana consomme à elle seule 5 % de l’approvisionnement annuel en eau, provoquant une pénurie urbaine chronique.
44. A Ciudad Juarez-El Paso, la pollution atmosphérique couvre les deux villes d’un épais brouillard.
45. Les problèmes sont tels que des infrastructures binationales de traitement des eaux usées et d’analyse de la qualité de l’air commencent à être mises en place.
46. A la détérioration des conditions de vie s’ajoutent la contrebande, la délinquance et l’insécurité. 
47. Des réseaux transfrontaliers organisent le trafic de drogue et la prostitution.
48. En effet, si les villes frontalières ont depuis longtemps leurs maisons closes et leurs réseaux de proxénètes dans cette “ zone de tolérance ”, la violence et le crime y ont pris des formes nouvelles.
49. Les cartels présents à Tijuana et à Ciudad Juarez, les deux centres les plus importants, contrôlent les exportations de drogue vers les Etats-Unis.
50. A Tijuana, les narcos auraient été responsables de 600 meurtres en 1997.
51. D’après le journal local Zeta, la guerre entre le Cartel de Tijuana dirigé par les frères Arellano Felix et la mafia de Sinaloa avait déjà provoqué plus de 300 assassinats, en juillet 1999, en Basse-Californie.
52. Car ce gouvernement invisible tend à se transnationaliser. 
53. Les deux groupes se disputent férocement le contrôle de la route côtière.
54. C’est à Tijuana que fut assassiné, le 23 mars 1994, Luis Donaldo Colosio, le candidat du Parti révolutionnaire institutionnel (PRI) à l’élection présidentielle mexicaine, et les mercenaires du Cartel Felix Arellano qui assassinèrent le cardinal Posadas Ocampo à Guadalajara avaient été recrutés dans des gangs qui sévissent à San Diego (Etats-Unis).
55. Les policiers des Etats concernés, commente Zeta, n’enquêtent pas sur les meurtres, soit par crainte des représailles, soit parce qu’ils sont complices.
56. De part et d’autre de la linea (la ligne : frontière juridique), pourtant, les gouvernements renforcent les effectifs militaires.
57. Car si, dans le monde de l’Alena, les marchandises (et la pollution) circulent librement - à cinq kilomètres à peine des postes de contrôle douaniers de San Diego- Tijuana, des embouteillages de camions rutilants transportant des marchandises passent la frontière sans problème -, il n’en est pas de même des hommes.
58. L’urbanisation accélérée et anarchique de la région s’étend sur un plateau au relief accidenté jusqu’au bord du Pacifique.
59. Là, le mur métallique d’environ trois mètres de haut qui longe désormais partiellement la frontière et sépare les villes jumelles s’enfonce dans l’océan tel un nouveau rideau de fer.
60. Depuis l’approbation, en 1996, d’une loi sur l’émigration illégale et la responsabilité des émigrants, les sans- papiers entrés illégalement aux Etats-Unis sont passibles d’amendes ou de peines de prison.
61. En février de cette année 1996, le gouvernement américain a décidé de recruter 1 000 agents supplémentaires chaque année, pour atteindre 10 000 hommes destinés à protéger la frontière.
62. Le budget du Service d’immigration et de naturalisation (SIN) fut alors porté à 2 600 millions de dollars, une augmentation de 72 % par rapport au budget trouvé par M. Clinton en 1993 en accédant au pouvoir.
63. Depuis sa mise en place, l’opération “ Gatekeeper ” a provoqué un “ bouclage ” sans précédent et militarisé la frontière.
64. Les jeeps des borders patrols se relaient jour et nuit, les hélicoptères survolent la côte. 
65. Avec l’aide du Pentagone, des détecteurs sismiques ont été installés qui permettent de déceler les moindres mouvements.
66. Un jeune Mexicain, qui a réussi son passage après cinq tentatives infructueuses, raconte comment, grâce à des caméras à infrarouges, des informaticiens traquent les émigrants sur leur écran comme dans un jeu vidéo.
67. Ces informations sont transmises à des policiers sur le terrain, équipés de radios et leur permettent de localiser les clandestins.
68. Cette militarisation n’a pas interrompu le flot migratoire. Certes, le nombre de clandestins passant la frontière en Californie a diminué de façon spectaculaire : de 531 689 émigrants illégaux appréhendés à San Diego en 1993, on est passé à 166 151 pour les huit premiers mois de l999 .
69. On n’en évalue pas moins à plus d’un demi-million le nombre de personnes rentrées illégalement aux Etats-Unis en 1998 .
70. Cette même année, les Etats-Unis ont rapatrié 20 000 mineurs mexicains entrés clandestinement sur leur territoire.
71. Si les candidats au départ sont toujours aussi nombreux, c’est à cause de la structure du marché du travail mexicain : un déficit d’un million d’emplois chaque année . 
72. L’Alena a été plus bénéfique pour les Etats-Unis que pour le Mexique; ses effets destructeurs sur l’économie paysanne expliquent en grande partie l’ampleur des migrations des campagnes mexicaines vers les Etats-Unis.
73. “ Pour les nouvelles générations, le marché de l’emploi n’a jamais été aussi sombre depuis des décennies”, constate le sous- secrétaire au travail de Mexico .
74. D’après la Banque du Mexique, les millions de Mexicains poussés par la pauvreté à s’exiler et résidant légalement ou illégalement, définitivement ou temporairement aux Etats-Unis, envoient chaque année près de 5,5 milliards de dollars pour atténuer le dénuement des leurs, demeurés au pays.
75. Ces transferts équivalaient en 1997 à 4,5 % de l’ensemble des revenus de l’exportation et à près de 55 % du chiffre d’affaires des maquiladoras .
76. Cet attrait forcé de l’eldorado américain fait la prospérité d’un syndicat d’un nouveau genre, le Coyote Inc. 
77. L’organisation de la contrebande des clandestins grâce aux services payants des coyotes ou polleros, réseaux de passeurs transfrontaliers, est devenue un négoce presque aussi rentable que le trafic de drogue.
78. Pour 650 dollars par personne, payables à l’arrivée (tarif minimum de part et d’autre de la frontière), on passe en un jour, à ses risques et périls.
79. Mais de Mexico City à Phoenix (Arizona) il en coûte 1 200 dollars, 12 000 pour un tarif collectif de groupe (dix au minimum).
80. Sans parler des guides sans scrupules qui rançonnent les fugitifs pour 1 500 ou 2 000 dollars avant de les abandonner dans la nature où ils sont traqués et souvent maltraités par les patrouilles militaires.
81. Pour éviter les contrôles, pour contourner le mur métallique érigé dans les principales villes frontalières, les émigrants, prenant tous les risques, cherchent à traverser en passant par les régions montagneuses ou désertiques.
82. En 1998, un groupe de 10 personnes est mort de soif dans le désert, à l’est de San Diego.
83. Parfois les patrouilles frontalières tirent : 89 fugitifs ont été tués ou blessés par balles en 1998 .
84. Côté mexicain, sur le Pacifique, un immense panneau blanc fixé sur la barde de fer recense la liste des morts depuis 1995.
85. Plus de 400 victimes sont mortes de faim, de soif ou d’épuisement. La plus jeune a quinze ans, la plus âgée quarante.
